// TODO: 1.Keyed collections
// Sets
const myArr = [1, 2, 3, 4, 5, 3, 5, 1];
const mySet = new Set(myArr);

console.log(myArr);
console.log(mySet);
console.log(mySet.add('String'));
console.log(mySet.add(false));
console.log(mySet.add([1, 2, 3]));
console.log(mySet.size);
console.log(mySet.delete(5));
console.log(mySet.has(2));

// Maps
const myCar = new Map([
  ['make', 'Ford'],
  ['model', 'Mustang'],
]);

myCar.set('year', 1969);
myCar.set('speed', '220km');
myCar.delete('speed');
console.log(myCar.get('year'));
// has and size are the same Set
console.log(myCar);

// TODO: 2.Work with Object
let car = {
  make: 'Ford',
  model: 'Mustang',
  year: 1969,
  details: {
    doors: 2,
    engin: '390-4V V8',
  },
  summery: function () {
    return `${this.model} make by ${this.make} in ${this.year}`;
  },
};

console.log(car.speed);
console.log(car['make']);
console.log(car.details.engin);
console.log(car.hasOwnProperty('make'));
console.log(car.summery());
console.log(car);

const person = new Object();
person.name = 'James';
person.age = 40;
person.gender = 'Male';
console.log(person);

// constructor function

function Car(make, model, year) {
  this.make = make;
  this.model = model;
  this.year = year;
  this.summery = function () {
    return `${this.model} make by ${this.make} in ${this.year}`;
  };
}

const ferrari = new Car('Ferrari', 'F8 Tributo', 2020);
const corvet = new Car('Chevrolet Corvet', 'C3', 1968);
console.log(corvet.model);
console.log(ferrari.summery());

function Person(name, age, gender) {
  this.name = name;
  this.age = age;
  this.gender = gender;
}

const james = new Person('James Bond', 40, 'Male');

// TODO: Details of the object model

//define Class
class Pet {
  constructor(name, age) {
    this.name = name;
    this.age = age;
  }
}
const dog = new Pet('Leo', 1);
console.log(dog);
Pet.prototype.gender = 'Male';
console.log(Pet.prototype);

Person.prototype.job = 'Programmer';
Person.prototype.showName = function () {
  console.log(`My name is ${this.name} and my job is ${Person.prototype.job}`);
};
let personTwo = new Person('Jeremy Clarkson', 61, 'Male');
personTwo.showName(); // My name is Amy
